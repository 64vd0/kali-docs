---
title: Switching Desktop Environments
description:
icon:
weight:
author: ["gamb1t", 64vd0]
---

During install a user may select whichever desktop environment that they prefer. However, when using the official VM this is not a possibility. In these cases, and many others, a user may wish to change their desktop environment.

Some examples of the widely-used desktop environments: Xfce, GNOME, KDE, LXDE, MATE, etc.
The current running Desktop Environment can be checked:
1. by using Environment Variables

```console
kali@kali:~$ echo $XDG_CURRENT_DESKTOP
```

The output will show the namen of the current desktop environment

2. by viewing the `*-session file` binary under **/usr/bin** directory

```console
kali@kali:~$ echo $XDG_CURRENT_DESKTOP
```
The output will give information regarding the system's desktop environment.

3. using the GUI, by searching for "About" you will see results with details regarding the current desktop environment.

Before switching the desktop environment, we will first update the system and install the `kali-desktop-*` [metapackage](/docs/general-use/metapackages/) for the given DE and update the default x-session-manager to be the one we will be using from now on. When we install KDE we will be asked which login manager to use. We will select "sddm" as we will have to replace KDE due to how it interacts with Xfce.

```console
kali@kali:~$ sudo apt update
kali@kali:~$
kali@kali:~$ sudo apt install -y kali-desktop-kde
kali@kali:~$
kali@kali:~$ sudo update-alternatives --config x-session-manager
kali@kali:~$
```

If we choose to install KDE, we have to remember a few conflicts that might come up. We _can_ have KDE installed alongside other DEs, however the way that the packages are currently configured there will be a few configuration conflicts. For example, when both KDE and Xfce are installed Xfce will not be able to have its cursor selected.

To work around this we will remove Xfce and only have KDE installed, and we do not advise having other DEs alongside it. Keep in mind that this only applies for KDE; you may have both Xfce and GNOME installed at the same time with no conflicts.

```console
kali@kali:~$ sudo apt purge --autoremove kali-desktop-xfce
kali@kali:~$
```

We will now reboot the system and make sure that all our changes were made properly.

If multiple desktop environments are installed on a system, they can also be switched at log in page by clicking on the Settings icon, selecting the desired desktop environment and then logging in. The following session should have the chosen desktop environment applied.
